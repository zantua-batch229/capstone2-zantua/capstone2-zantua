const  Products = require("../models/Products.js");
const auth = require("../auth.js");


module.exports.addProduct = async(userData, reqBody) => {
	
	
	let isProductExists = await Products.find({name: reqBody.name}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})

	if (isProductExists){
		return "Product exists.";
	} else {
		if (userData.isAdmin) {
			let newProduct = new Products({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				salePrice: reqBody.salePrice,
				stock: reqBody.stock,
				image: reqBody.image,
				isActive: reqBody.isActive,
				isOnSale: reqBody.isOnSale
				
			})
			return newProduct.save().then((product, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})
		} else {
			return false;
		};
		
	}
}

module.exports.updateProduct = (productId, data) => {

	return Products.findByIdAndUpdate(productId, data).then((result, error) => {
			if(error){
				return false;
			} else {
				return true;
			}

		})

}

module.exports.getActiveProducts = () => {
	return Products.find({isActive:true}).then(result => {
		//if(result.length>0){
			return result;
		//}
	})
}
module.exports.getAllProducts = () => {
	return Products.find().then(result => {
		//if(result.length>0){
			return result
		//}
	})
}
module.exports.getProductDetails = (reqParms) => {
	let productId = reqParms.productId;
	return Products.findById(productId).then(result => {
		return result;
	})
}
module.exports.archiveProduct = (data) => {
	if(data.isAdmin === true){

		let updateActiveField = {
			isActive : false
		};
		return Products.findByIdAndUpdate(data.productId, updateActiveField).then((result, error) => {
			if(error){
				return false;
			} else {
				return true;
			}

		})
	} else {
		return false;
	}

}




