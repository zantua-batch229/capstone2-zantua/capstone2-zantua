const  Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const auth = require("../auth.js");

module.exports.getSales = (filter) => {
	return Orders.find({
        $and: [
            {
                purchasedOn: {$gte: new Date(new Date(filter.fromDate).setHours(00, 00, 00)) }
            },
            {
                purchasedOn:{$lte: new Date(new Date(filter.toDate).setHours(00, 00, 00))}
            }
        ]
    })
		.then(result => {
            let sales =[];
            let total =0;
            let count = 0;
            result.forEach(function(doc, index) { 
                sales.push((doc.purchasedOn + " Sub-Total: " + doc.totalAmount));
                total += doc.totalAmount;
                count += 1;
            });
            sales.push("TOTAL SALES: " + total);
            sales.push("NUMBER OF TRANSACTIONS: " + count);

		return sales;
	})
}