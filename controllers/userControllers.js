const  User = require("../models/Users.js");
const  Products = require("../models/Products.js");
const  Orders = require("../models/Orders.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(findResult => {
		if(findResult.length >0){
			return true;
		} else {
			return false;
		}
	})

}

module.exports.registerUser = async(reqBody) => {
	
	let isEmailTaken = await User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})

	if (isEmailTaken){
		return false;
		console.log("email is taken.")
	} else {
		
		let newUser = new User({
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10),
			// isAdmin: reqBody.isAdmin
			firstName : reqBody.firstName,
			lastName : reqBody.lastName,
			mobileNo : reqBody.mobile,

		})
		return newUser.save().then((user, error) => {
			// registration failed
			if(error){
				return false;
			}else{
				// if registration is successfull
				return true;
			}
		})
		
	}


		
	
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

module.exports.setUserAsAdmin = (userId, admin) => {
	return User.findByIdAndUpdate(userId, {isAdmin: true}).then((result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.unsetUserAsAdmin = (userId, admin) => {
	return User.findByIdAndUpdate(userId, {isAdmin: false}).then((result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.getUsers = () => {
	return User.find().then(result => {
		return result;
	})
}
module.exports.getUsersDetails = (userId_rec) => {
	return User.findById(userId_rec).then(result => {
		return result;
	})
}

module.exports.userUpdate = (userId_rec, data) => {
	let updatedData = {
		email: data.email,
		password: bcrypt.hashSync(data.password, 10)
	};
	return User.findByIdAndUpdate(userId_rec, updatedData).then((result, error) => {
		if (error){
			return false;
		} else {
			return true;
		}
	})
}
