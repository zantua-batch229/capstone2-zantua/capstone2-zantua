const  OrderStatus = require("../models/OrderStatuses.js");
const Orders = require("../models/Orders.js");
const auth = require("../auth.js");

module.exports.updateStatus = (data) => {
	return Orders.findByIdAndUpdate(data.orderId, {status : data.status}).then((result, error) => {
		if (error){
			return false;
		} else {
			
			let newStatus = new OrderStatus({
				status : data.status,
				remarks: data.remarks,
				orderId: data.orderId,
				updatedBy: data.userId
			});

			return newStatus.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})

		}

	})
}
module.exports.getOrderStatus = (orderId_rec, userId) => {
	return OrderStatus.find({orderId: orderId_rec}).then(result => {
		return result;
	})
}