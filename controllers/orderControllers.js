const Orders = require("../models/Orders.js");
const Users = require("../models/Users.js");
const auth = require("../auth.js");

module.exports.getAllOrders = () => {
	return Orders.find().then(result => {
			return result;
	})

}

module.exports.getUserOrders = (userId_orders) => {
	return Orders.find({userId : userId_orders}).then(result => {
		return result;
	})
}


