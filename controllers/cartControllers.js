const CartItems = require("../models/Carts.js");
const Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const Users = require("../models/Users.js");
const auth = require("../auth.js");


module.exports.addToCart = async(data) => {
	
	let productId = data.productId;
	let userId = data.userId;
	let isProductExists = false;
	let itemPrice = 0;
	
	let findProduct = await Products.find({_id: productId});

	if(findProduct.length > 0){
		isProductExists = true;
		itemPrice=findProduct[0].price;
	}

	let isUserExists = await Users.find({_id: userId}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
	let originalQty = 0;

	let isProductAddedtoCart = await CartItems.find({$and: [{userId}, {productId: productId}]}).then(result => {
		if(result.length > 0){
 			originalQty = result[0].quantity;
			return true;
		}else{
			return false;
		}

	})

	let newQty = originalQty + data.quantity;


	if(isProductExists && isUserExists){

		if(isProductAddedtoCart){

			return  CartItems.findOneAndUpdate({$and: [{userId: userId},{productId: productId}]},
    		{quantity: newQty}).then((item, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})			

		} else {

			let addProduct = new CartItems({
				productId: data.productId,
				userId: data.userId,
				quantity: data.quantity,
				unitPrice: itemPrice});

			return addProduct.save().then((item, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})

		}

	} else {
		return false;
	}
}


module.exports.checkout = async(userId_carts) =>{
	let itemsInCart = [];
	let subTotal = 0;

	// get items from cart docs with userId
	let getProduct = await CartItems.find({userId: userId_carts}).then((result, error)=>{
		return result;	
	})

	// get relevant info from result and create an object array(itemsInCart) and total or orders
	getProduct.forEach(function(product){
		let eachProduct = {
			productId: product.productId,
			quantity: product.quantity,
			unitPrice: product.unitPrice 
		};
		subTotal += product.quantity * product.unitPrice;
		itemsInCart.push(eachProduct);
	});

	// prepare an order entry
	let addOrder = new Orders({

		userId: userId_carts,
		totalAmount: subTotal,
		products: itemsInCart
	});
	// save the entry
	return addOrder.save().then((item, error) => {
		if(error){
			return false;
		} else {
			// delete docs from cart
			return CartItems.deleteMany({userId: userId_carts}).then((removedItems, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})
			
		}
	})

}
module.exports.removeItem = async (UserId,cartId) => {
	let isUserOwnerOfItem = await CartItems.findById(cartId).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
	if(isUserOwnerOfItem){
		return CartItems.findByIdAndRemove(cartId).then((removedItem, error)=>{
			if(error){
				return false;
			}else{
				return removedItem;
			}

		})

	} else {
		return false;
	}
		
}

module.exports.getAllCartItems = (userId_items) => {
	return CartItems.find({userId: userId_items}).then(result => {
			return result
	})

}

module.exports.updateQuantity = (data) => {
	return CartItems.findOneAndUpdate({
		// find
        $and: [{
                	userId: data.userId
            	},
	            {
	                productId: data.productId 
	            }
        	]},
        // update
    		{quantity: data.quantity})
    .then((editedItem, error)=>{
		if(error){
			return false;
		}else{
			return editedItem;
		}

	})
}