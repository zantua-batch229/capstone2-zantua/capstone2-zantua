const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({

	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	totalAmount: {
		type: Number,

	},
	status: {
		type: String,
		default: "pending"
	},
	

	products: [{
		productId: {
			type: String,
			required: [true, "productId is required"]
		},

		quantity: {
			type: Number
		},

		unitPrice: {
			type: Number
		}
	}]


})
module.exports = mongoose.model("Order", orderSchema);
