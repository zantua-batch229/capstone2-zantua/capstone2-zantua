const mongoose = require("mongoose");

const messageSchema = mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},

	name: {
		type: String
		
	},
	message: {
		type: String
		
	},
	mobile: {
		type: String
	},
	date: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Message", messageSchema);