const mongoose = require("mongoose");

const cartSchema = mongoose.Schema({

	dateAdded: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	productId: {
		type: String,
		required: [true, "Product Id is required"]
	},
	quantity: {
		type: Number
	},
	unitPrice: {
		type: Number
	}
})

module.exports = mongoose.model("Cart", cartSchema);