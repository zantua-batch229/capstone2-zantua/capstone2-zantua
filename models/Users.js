const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	mobile: {
		type: String,
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isUserAdmin: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model("User", userSchema);