const mongoose = require("mongoose");

const statusSchema = mongoose.Schema({
	dateUpdated: {
		type: Date,
		default: new Date()
	},
	updatedBy: {
		type: String
	},
	status: {
		type: String
	},
	remarks: {
		type: String
	}
});

module.exports = mongoose.model("OrderStatus", statusSchema);