const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
	},
	price: {
		type: Number
	},
		salePrice: {
		type: Number
	},
	image: {
		type: String
	},
	stock: {
		type: Number
	},

	isActive: {
		type: Boolean,
		default: true
	},
	isOnSale: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	reviews: [{
		user: {
			type: String
		},
		stars: {
			type: Number
		},
		remarks: {
			type: String
		}
	}]

});
module.exports = mongoose.model("Product", productSchema);