const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const statusController = require("../controllers/statusControllers.js");
const auth	= require("../auth.js");

router.post("/:orderId/update", auth.verify, (req, res)=>{

	if(auth.decode(req.headers.authorization).isAdmin === true){
		let data = {
			orderId: req.params.orderId,
			remarks: req.body.remarks,
			status: req.body.status,
			userId: auth.decode(req.headers.authorization).id

		};

			statusController.updateStatus(data).then(resultFromController => res.send(resultFromController));
	} else {
			res.send("Only admins can update status");
	}

})

router.get("/:orderId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		statusController.getOrderStatus(req.params.orderId, "admin").then(resultFromController => res.send(resultFromController))
	} else {
		
		
		res.send("Only Admins can view status history")
	}
})






module.exports = router;