const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const cartController = require("../controllers/cartControllers.js");
const orderController = require("../controllers/orderControllers.js");
const auth	= require("../auth.js");


router.get("/", auth.verify, (req, res)=> {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Only admin can retrive orders");
	}
})

router.get("/:userId", (req, res) => {
	orderController.getUserOrders(req.params.userId).then(resultFromController => res.send(resultFromController))
})

router.put("/:orderId/update", auth.verify, (req, res)=>{

	if(auth.decode(req.headers.authorization).isAdmin === true){
			orderController.updateStatus(req.params.orderId, req.body.status).then(resultFromController => res.send(resultFromController));
	} else {
			res.send("Only admins can update status");
	}

})


module.exports = router;