const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const messageController = require("../controllers/messageControllers.js");

const auth	= require("../auth.js");


router.get("/", auth.verify, (req, res)=> {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		messageController.getAllMessages().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Only admin can retrive messages");
	}
})

router.post("/send", (req, res) => {
	messageController.sendMessage(req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;