const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js")
const auth = require("../auth.js")



router.get("/", (req, res) => {
	productControllers.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/allproducts", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		productControllers.getAllProducts().then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Only Admins can see ALL products");
	}
})

router.post("/add", auth.verify, (req, res) => {
	
	const userData = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.addProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.archiveProduct(data).then(resultFromController => res.send(resultFromController));
})
router.put("/:productId/update", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		const data = {
			name : req.body.name,
			price : req.body.price,
			salePrice : req.body.salePrice,
			isOnSale : req.body.isOnSale,
			stock : req.body.stock,
			description : req.body.description, 
			isActive : req.body.isActive,
			image : req.body.imageURL
		}
		productControllers.updateProduct(req.params.productId, data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send("User not admin");
	}
})

router.get("/:productId", (req, res) => {
	
		productControllers.getProductDetails(req.params).then(resultFromController => res.send(resultFromController));

})




module.exports = router;