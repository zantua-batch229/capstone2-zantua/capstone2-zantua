const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")
const auth = require("../auth.js")




router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})
router.put("/:userId/setadmin", auth.verify, (req, res)=> {

	if(auth.decode(req.headers.authorization).isUserAdmin === true){

		userControllers.setUserAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Only Super Admins can change user status");
	}
})
router.put("/:userId/unsetadmin", auth.verify, (req, res)=> {

	if(auth.decode(req.headers.authorization).isUserAdmin === true){

		userControllers.unsetUserAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Only Super Admins can change user status");
	}
})
router.get("/", auth.verify, (req, res)=> {

	if(auth.decode(req.headers.authorization).isUserAdmin === true){

		userControllers.getUsers(req.params.userId).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Only Super Admins can view users");
	}
})

router.get("/details", auth.verify, (req, res)=> {
	
	userControllers.getUsersDetails(auth.decode(req.headers.authorization).id).then(resultFromController => res.send(resultFromController))
	
})

router.put("/update", auth.verify, (req, res)=> {
	const updatedData = {
		email: req.body.email,
		password: req.body.password
	};
	userControllers.userUpdate(auth.decode(req.headers.authorization).id, updatedData).then(resultFromController => res.send(resultFromController))
	
})


module.exports = router;