const express = require("express");
const auth = require("../auth.js")
const router = express.Router();
const reportControllers = require("../controllers/reportControllers.js")

router.get("/sales", auth.verify, (req, res)=> {
	if (auth.decode(req.headers.authorization).isAdmin ===true){
		const reportFilter = {
			fromDate: req.body.fromDate,
			toDate: req.body.toDate
		};
		reportControllers.getSales(reportFilter).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Only Admins are allowed to view reports")
	}
	
})


module.exports = router;

