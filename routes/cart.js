const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const cartController = require("../controllers/cartControllers.js");
const orderController = require("../controllers/orderControllers.js");
const auth	= require("../auth.js");


router.post("/add", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		res.send("Admins cannot add to cart");
	} else {
		const data = {
			productId: req.body.productId,
			userId: auth.decode(req.headers.authorization).id,
			quantity: req.body.quantity
		}

		cartController.addToCart(data).then(resultFromController => res.send(resultFromController))

	}
})

router.delete("/:cartItemId/remove", auth.verify, (req, res)=>{
	cartController.removeItem(auth.decode(req.headers.authorization).id, req.params.cartItemId).then(resultFromController => res.send(resultFromController))
})

router.get("/:userId", (req, res) => {
	cartController.getAllCartItems(req.params.userId).then(resultFromController => res.send(resultFromController))
})

router.put("/editqty", auth.verify, (req, res) => {
	const data = {
		productId: req.body.productId,
		userId: auth.decode(req.headers.authorization).id,
		quantity: req.body.quantity
	}

	cartController.updateQuantity(data).then(resultFromController => res.send(resultFromController))
})

router.post("/checkout", auth.verify, (req, res)=>{
	cartController.checkout(auth.decode(req.headers.authorization).id).then(resultFromController => res.send(resultFromController))
})

router.put("/editqty", auth.verify, (req, res) => {
	const data = {
		productId: req.body.productId,
		userId: auth.decode(req.headers.authorization).id,
		quantity: req.body.quantity
	}

	cartController.updateQuantity(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
