//run nodemon index.js on git bash
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product.js");
const cartsRoutes = require("./routes/cart.js");
const orderRoutes = require("./routes/order.js");
const statusRoutes = require("./routes/order_status.js");
const reportRoutes = require("./routes/reports.js");
const messageRoutes = require("./routes/message.js");

const app = express();

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.tmdxo2p.mongodb.net/capstone2?retryWrites=true&w=majority", {

	useNewUrlParser : true,
	useUnifiedTopology : true

});

// set notification for connection success or failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// MiddleWares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartsRoutes);
app.use("/orders", orderRoutes);
app.use("/status", statusRoutes);
app.use("/reports", reportRoutes);
app.use("/messages", messageRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});